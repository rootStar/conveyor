
-record(pool, {
    name,
    belts = 1,
    max_belts = 0,
    pool_strategy = lifo,
    produce_interval = false,
    rate_limit = 0,
    init = fun(_Pool) -> [] end,
    exec = fun(_State, _Data) -> ok end,
    term = fun(_Pool, _State) -> ok end,
    pipe_each = false,
    pipe_type = sync,
    pipe_to = false,
    worker_data = []
}).
