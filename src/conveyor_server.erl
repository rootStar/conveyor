%%% @author   Nick Roshin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roshin.
%%% @doc

-module(conveyor_server).

-behaviour(gen_server).

-define(SERVER, ?MODULE).

-author('Nick Roshin <deneb808@gmail.com>').

%% ===================================================================
%% API Function Exports
%% ===================================================================

-export([
    start_link/0,
    enqueue/3,
    start/1,
    stop/1
]).


%% ===================================================================
%% gen_server Function Exports
%% ===================================================================

-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    terminate/2,
    handle_info/2,
    code_change/3
]).

-define(WATCH_TIMEOUT, 1000).

-define(WAIT_WORKER, 60000).

-include("header.hrl").

%% ===================================================================
%% API Function Definitions
%% ===================================================================

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

enqueue(_Type, false, _Data) ->
    ok;
enqueue(async, Name, Data) ->
    WorkerPid = poolboy:checkout(Name, true, infinity),
    conveyor_belt:execute_async(WorkerPid, Data),
    poolboy:checkin(Name, WorkerPid);
enqueue(sync, Name, Data) ->
    WorkerPid = poolboy:checkout(Name, true, infinity),
    Result = conveyor_belt:execute_sync(WorkerPid, Data, ?WAIT_WORKER),
    poolboy:checkin(Name, WorkerPid),
    Result.

start(Name) when is_atom(Name) ->
    gen_server:call(?MODULE, {start, #pool{ name = Name } });
start(Pool) when is_record(Pool, pool) ->
    gen_server:call(?MODULE, {start, Pool}).

stop(Name) when is_atom(Name) ->
    gen_server:call(?MODULE, {stop, #pool{ name = Name }});
stop(Pool) when is_record(Pool, pool) ->
    gen_server:call(?MODULE, {stop, Pool}).

%% ===================================================================

init([]) ->
    {ok, []}.


handle_call({start, #pool{name = Name} = Pool}, _From, Timers) ->
    NewTimers = case add_conveyor(Pool) of
        {ok, _Pid} ->
            {ok, TimerRef} = timer:send_interval(?WATCH_TIMEOUT, {watch_pool, Pool}),
            [{Name, TimerRef} | Timers];
        {error, Reason} ->
            error_logger:error_msg("Error ~p~n", [Reason]),
            Timers
    end,
    {reply, ok, NewTimers};

handle_call({stop, #pool{name = Name} = Pool}, _From, Timers) ->
    NewTimers = case del_conveyor(Pool) of
        ok ->
            timer:cancel(proplists:get_value(Name, Timers)),
            proplists:delete(Name, Timers);
        {error, Reason} ->
            io:format("Error ~p~n", [Reason]),
            Timers
    end,
    {reply, ok, NewTimers};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({watch_pool, Pool}, State) ->
    {noreply, State};

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ===================================================================
%% Internal Function Definitions
%% ===================================================================

add_conveyor(#pool{ name = Name } = Pool) ->
    supervisor:start_child(conveyor_sup, {
        Name,
        {poolboy, start_link, [pool_sup_args(Pool), Pool]},
        transient, 5000, worker, [poolboy]
    }).

del_conveyor(#pool{name = Name}) ->
    ok = supervisor:terminate_child(conveyor_sup, Name),
    supervisor:delete_child(conveyor_sup, Name).

pool_sup_args(#pool{name = Name, belts = Belts, max_belts = MaxBelts, pool_strategy = Strategy}) ->
    [
        {name, {local, Name}},
        {worker_module, conveyor_belt},
        {size, Belts},
        {strategy, Strategy},
        {max_overflow, MaxBelts - Belts}
    ].
