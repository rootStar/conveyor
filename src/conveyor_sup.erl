%%%-------------------------------------------------------------------
%% @doc conveyor top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(conveyor_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-include("header.hrl").

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    {ok, {
            {one_for_all, 5, 10},
            [{
                conveyor_server,
                {conveyor_server, start_link, []},
                permanent,
                5000,
                worker,
                [conveyor_server]
            }]
        }
    }.

%%====================================================================
%% Internal functions
%%====================================================================
