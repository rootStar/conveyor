-module(conveyor).


-export([
    start/1,
    stop/1,
    sync_enqueue/2,
    async_enqueue/2
]).


start(Pool) ->
    conveyor_server:start(Pool).

stop(Pool) ->
    conveyor_server:stop(Pool).

sync_enqueue(Name, Data) ->
    conveyor_server:enqueue(sync, Name, Data).

async_enqueue(Name, Data) ->
    conveyor_server:enqueue(async, Name, Data).
