%%% @author   Nick Roshin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roshin.
%%% @doc

-module(conveyor_belt).

-behaviour(gen_server).

-define(SERVER, ?MODULE).

-author('Nick Roshin <deneb808@gmail.com>').

%%===================================================================
%% API Function Exports
%%===================================================================

-export([
    start_link/1,
    execute_async/2,
    execute_sync/3
]).

%%===================================================================
%% gen_server Function Exports
%%===================================================================

-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    terminate/2,
    handle_info/2,
    code_change/3
]).

-include("header.hrl").

-record(state, { pool, worker_state }).
%%===================================================================
%% API Function Definitions
%%===================================================================

start_link(Pool) ->
    gen_server:start_link(?MODULE, Pool, []).

execute_async(Pid, Data) ->
    gen_server:cast(Pid, {execute, Data}).

execute_sync(Pid, Data, Timeout) ->
    gen_server:call(Pid, {execute, Data}, Timeout).

%%===================================================================
%% gen_server Function Definitions
%%===================================================================

init(Pool) ->
    process_flag(trap_exit, true),
    gen_server:cast(self(), init),
    {ok, #state{pool = Pool}}.


handle_call({execute, Data}, _From, State) ->
    {WState, Result} = execute(State, Data),
    {reply, Result, State#state{ worker_state = WState }};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.


handle_cast(init, #state{ pool = #pool{ init = Init, produce_interval = false } = Pool } = State) ->
    {noreply, State#state{ worker_state = Init(Pool) }};

handle_cast(init, #state{ pool = #pool{ init = Init, produce_interval = Interval } = Pool } = State) ->
    erlang:send_after(Interval, self(), self_produce),
    {noreply, State#state{ worker_state = Init(Pool) }};

handle_cast({execute, Data}, State) ->
    {WState, _} = execute(State, Data),
    {noreply, State#state{ worker_state = WState }};

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(self_produce, #state{ pool = #pool{ produce_interval = Interval } } = State) ->
    {WState, _} = execute(State, []),
    erlang:send_after(Interval, self(), self_produce),
    {noreply, State#state{ worker_state = WState }};


handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #state{pool = #pool{term = Term} = Pool, worker_state = WState}) ->
    Term(Pool, WState).

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%===================================================================
%% Internal Function Definitions
%%===================================================================

rate_limit(Ececuted, SleepTime) when SleepTime >= Ececuted ->
    timer:sleep(SleepTime - Ececuted);
rate_limit(_Ececuted, _SleepTime) ->
    ok.

execute(#state{ pool = Pool, worker_state = WState }, Params) ->
    #pool{ exec = Exec, rate_limit = RLimit } = Pool,
    Start = erlang:system_time(milli_seconds),
    case try_execute(Exec, [WState | Params]) of
        {error, Reason} ->
            error_logger:error_msg("Error ~p~n", [Reason]);
        Result ->
            rate_limit(erlang:system_time(milli_seconds) - Start, RLimit),
            Return = {_, Res} = process_results(Result, WState),
            pipe_next(Pool, Res),
            Return
    end.

pipe_next(#pool{pipe_each = true}, []) ->
    ok;
pipe_next(#pool{pipe_each = true} = Pool, [First | Tail]) ->
    pipe_next(Pool#pool{pipe_each = false}, First),
    pipe_next(Pool, Tail);
pipe_next(#pool{pipe_to = PipeTo, pipe_type = PipeType, pipe_each = false}, Data) ->
    conveyor_server:enqueue(PipeType, PipeTo, [Data]).


process_results(Return, WorkerState) ->
    case Return of
        {save, State, Result} ->
            {State, Result};
        _ ->
            {WorkerState, Return}
    end.

try_execute(Fun, Data) when is_function(Fun) ->
    try apply(Fun, Data) of
        Result ->
            Result
    catch
        Error ->
            {error, Error}
    end;
try_execute({M,F}, Data) when is_atom(M) andalso is_atom(F) ->
    try apply(M, F, Data) of
        Result ->
            Result
    catch
        Error ->
            {error, Error}
    end.
