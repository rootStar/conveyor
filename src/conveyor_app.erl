%%%-------------------------------------------------------------------
%% @doc conveyor public API
%% @end
%%%-------------------------------------------------------------------

-module(conveyor_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-include("header.hrl").

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    conveyor_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
